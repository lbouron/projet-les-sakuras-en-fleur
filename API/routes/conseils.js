let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/conseils');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /conseils:
     *   get:
     *     tags:
     *       - Conseils
     *     summary: Retourne tous les conseils
     *     description: Retourne tous les conseils
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       500:
     *         description: Erreur interne
     */
    router.get('/conseils', Controller.getAll);

    /**
     * @swagger
     * /conseils/{id}:
     *   get:
     *     tags:
     *       - Conseils
     *     summary: Retourne les infos d'un conseil
     *     description: Retourne les infos d'un conseil
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant du conseil
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       404:
     *         description: Aucun résultat pour ce conseil
     *       500:
     *         description: Erreur interne
     */
    router.get('/conseils/:id', Controller.getConseilById);

    /**
     * @swagger
     * /conseils/add:
     *   put:
     *     tags:
     *       - Conseils
     *     summary: Ajoute un conseil
     *     description: Ajoute un conseil
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Conseil à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *             contenu:
     *               type: text
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       500:
     *         description: Erreur interne
     */
    router.put('/conseils/add', allowOnly(config.userRoles.admin, Controller.add));

    /**
     * @swagger
     * /conseils/update/{id}:
     *   post:
     *     tags:
     *       - Conseils
     *     summary: Met à jour un conseil
     *     description: Met à jour un conseil
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Conseil à modifier
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *             contenu:
     *               type: text
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour ce conseil
     *       500:
     *         description: Erreur interne
     */
    router.post('/conseils/update/:id', allowOnly(config.userRoles.admin, Controller.updateById));

    /**
     * @swagger
     * /conseils/delete/{id}:
     *   delete:
     *     tags:
     *       - Conseils
     *     summary: Supprime un conseil
     *     description: Supprime un conseil
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Conseil à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour ce conseil
     *       500:
     *         description: Erreur interne
     */
    router.delete('/conseils/delete/:id', allowOnly(config.userRoles.admin, Controller.deleteById));

    return router;
}

module.exports = APIRoutes;