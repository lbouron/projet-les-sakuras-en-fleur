let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/commandes');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /commandes:
     *   get:
     *     tags:
     *       - Commandes
     *     summary: Retourne toutes les commandes
     *     description: Retourne toutes les commandes
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       500:
     *         description: Erreur interne
     */
    router.get('/commandes', allowOnly(config.userRoles.admin, Controller.getAll));

    /**
     * @swagger
     * /commandes/{id}:
     *   get:
     *     tags:
     *       - Commandes
     *     summary: Retourne les infos d'une commande
     *     description: Retourne les infos d'une commande
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant de la commande
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour cette commande
     *       500:
     *         description: Erreur interne
     */
    router.get('/commandes/:id', allowOnly(config.userRoles.admin, Controller.getCommandeById));

    /**
     * @swagger
     * /commandes/add:
     *   put:
     *     tags:
     *       - Commandes
     *     summary: Ajoute une commande
     *     description: Ajoute une commande
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Commande à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             total:
     *               type: decimal
     *             statut:
     *               type: string
     *             utilisateurId:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.put('/commandes/add', allowOnly(config.userRoles.user, Controller.add));

    /**
     * @swagger
     * /commandes/update/{id}:
     *   post:
     *     tags:
     *       - Commandes
     *     summary: Met à jour une commande
     *     description: Met à jour une commande
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Commande à modifier
     *         schema:
     *           type: object
     *           properties:
     *             total:
     *               type: decimal
     *             statut:
     *               type: string
     *             utilisateurId:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour cette commande
     *       500:
     *         description: Erreur interne
     */
    router.post('/commandes/update/:id', allowOnly(config.userRoles.admin, Controller.updateById));

    /**
     * @swagger
     * /commandes/delete/{id}:
     *   delete:
     *     tags:
     *       - Commandes
     *     summary: Supprime une commande
     *     description: Supprime une commande
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Commande à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour cette commande
     *       500:
     *         description: Erreur interne
     */
    router.delete('/commandes/delete/:id', allowOnly(config.userRoles.admin, Controller.deleteById));

    /**
     * @swagger
     * /commandes/user/{userId}:
     *   get:
     *     tags:
     *       - Commandes
     *     summary: Retourne toutes les commandes de l'utilisateur connecté
     *     description: Retourne toutes les commandes de l'utilisateur connecté
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       401:
     *         description: Utilisateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       500:
     *         description: Erreur interne
     */
    router.get('/commandes/user/:userId', allowOnly(config.userRoles.user, Controller.getCommandesByUser));

    return router;
}

module.exports = APIRoutes;