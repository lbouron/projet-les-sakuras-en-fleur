let router = require('express').Router();

const config = require('../config/config');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/categories');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /categories:
     *   get:
     *     tags:
     *       - Catégories
     *     summary: Retourne toutes les catégories
     *     description: Retourne toutes les catégories
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       500:
     *         description: Erreur interne
     */
    router.get('/categories', Controller.getAll);

    /**
     * @swagger
     * /categories/{id}:
     *   get:
     *     tags:
     *       - Catégories
     *     summary: Retourne les infos d'une catégorie
     *     description: Retourne les infos d'une catégorie
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant de la catégorie
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       404:
     *         description: Aucun résultat pour cette catégorie
     *       500:
     *         description: Erreur interne
     */
    router.get('/categories/:id', Controller.getCategorieById);

    /**
     * @swagger
     * /categories/add:
     *   put:
     *     tags:
     *       - Catégories
     *     summary: Ajoute une catégorie
     *     description: Ajoute une catégorie
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Catégorie à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       500:
     *         description: Erreur interne
     */
    router.put('/categories/add', allowOnly(config.userRoles.admin, Controller.add));

    /**
     * @swagger
     * /categories/update/{id}:
     *   post:
     *     tags:
     *       - Catégories
     *     summary: Met à jour une catégorie
     *     description: Met à jour une catégorie
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Catégorie à modifier
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour cette catégorie
     *       500:
     *         description: Erreur interne
     */
    router.post('/categories/update/:id', allowOnly(config.userRoles.admin, Controller.updateById));

    /**
     * @swagger
     * /categories/delete/{id}:
     *   delete:
     *     tags:
     *       - Catégories
     *     summary: Supprime une catégorie
     *     description: Supprime une catégorie
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Catégorie à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour cette catégorie
     *       500:
     *         description: Erreur interne
     */
    router.delete('/categories/delete/:id', allowOnly(config.userRoles.admin, Controller.deleteById));

    return router;
}

module.exports = APIRoutes;