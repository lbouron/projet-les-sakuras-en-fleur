let router = require('express').Router();

const config = require('../config/config');
const upload = require('../services/upload');
const allowOnly = require('../services/route').allowOnly;

const Controller = require('../controllers/produits');

let APIRoutes = function (passport) {
    /**
     * @swagger
     * /produits:
     *   get:
     *     tags:
     *       - Produits
     *     summary: Retourne tous les produits
     *     description: Retourne tous les produits
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Liste
     *       500:
     *         description: Erreur interne
     */
    router.get('/produits', Controller.getAll);

    /**
     * @swagger
     * /produits/{id}:
     *   get:
     *     tags:
     *       - Produits
     *     summary: Retourne les infos d'un produit
     *     description: Retourne les infos d'un produit
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: id
     *         description: Identifiant du produit
     *         in: body
     *         required: true
     *         type: int
     *     responses:
     *       200:
     *         description: Succès
     *       404:
     *         description: Aucun résultat pour ce produit
     *       500:
     *         description: Erreur interne
     */
    router.get('/produits/:id', Controller.getProduitById);

    /**
     * @swagger
     * /produits/add:
     *   put:
     *     tags:
     *       - Produits
     *     summary: Ajoute un produit
     *     description: Ajoute un produit
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Produit à ajouter
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *             description:
     *               type: text
     *             prix:
     *               type: decimal
     *             image:
     *               type: blob
     *     responses:
     *       200:
     *         description: Succès
     *       400:
     *         description: Champs obligatoires manquants
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       500:
     *         description: Erreur interne
     */
    router.put('/produits/add', upload.any(), allowOnly(config.userRoles.admin, Controller.add));

    /**
     * @swagger
     * /produits/update/{id}:
     *   post:
     *     tags:
     *       - Produits
     *     summary: Met à jour un produit
     *     description: Met à jour un produit
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: entry
     *         description: Produit à modifier
     *         schema:
     *           type: object
     *           properties:
     *             libelle:
     *               type: string
     *             description:
     *               type: text
     *             prix:
     *               type: decimal
     *             image:
     *               type: blob
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour ce produit
     *       500:
     *         description: Erreur interne
     */
    router.post('/produits/update/:id', upload.any(), allowOnly(config.userRoles.admin, Controller.updateById));

    /**
     * @swagger
     * /produits/delete/{id}:
     *   delete:
     *     tags:
     *       - Produits
     *     summary: Supprime un produit
     *     description: Supprime un produit
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: entry
     *         description: Produit à supprimer
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Administrateur non connecté
     *       403:
     *         description: Permissions insuffisantes
     *       404:
     *         description: Aucun résultat pour ce produit
     *       500:
     *         description: Erreur interne
     */
    router.delete('/produits/delete/:id', allowOnly(config.userRoles.admin, Controller.deleteById));

    /**
     * @swagger
     * /produits/image/{id}:
     *   get:
     *     tags:
     *       - Produits
     *     summary: Retourne l'image d'un produit
     *     description: Retourne l'image d'un produit
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: params
     *         name: id
     *         description: Entrée désirée
     *         type: integer
     *     responses:
     *       200:
     *         description: Succès
     *       401:
     *         description: Utilisateur non connecté
     *       500:
     *         description: Erreur interne
     */
    router.get('/produits/image/:id', Controller.getImage);

    return router;
}

module.exports = APIRoutes;