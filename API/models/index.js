exports.utilisateurs    = require('./utilisateurs');
exports.commandes       = require('./commandes');
exports.produits        = require('./produits');
exports.conseils        = require('./conseils');
exports.commandeContenu = require('./commandeContenu');
exports.categories      = require('./categories');