const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    libelle: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        validate: {
            notNull: {
                msg: "Le libellé de l'article ne peut pas être vide."
            },
            notEmpty: {
                msg: "Le libellé doit être renseigné."
            },
            len: {
                args: [1, 250],
                msg: "Le libellé doit faire entre 1 et 250 caractères."
            }
        }
    },
    description: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: true
    },
    prix: {
        type: Sequelize.DataTypes.DECIMAL,
        allowNull: false,
        validate: {
            min: {
                args: [0],
                msg: "Le prix doit être positif."
            },
            isDecimal: {
                msg: "Le prix doit être un nombre décimal."
            },
            notNull: {
                msg: "Le prix ne peut pas être vide."
            }
        }
    },
    image: {
        type: Sequelize.DataTypes.BLOB,
        allowNull: true
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let produitsModel = db.define('produits', modelDefinition, modelOptions);

module.exports = produitsModel;