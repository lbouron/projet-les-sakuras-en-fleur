const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    total: {
        type: Sequelize.DataTypes.DECIMAL,
        allowNull: false,
        validate: {
            isDecimal: {
                msg: "La valeur doit être un nombre décimal."
            }
        }
    },
    statut: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        defaultValue: "Validée",
        validate: {
            isIn: {
                args: [[ "Annulée", "Validée", "Payée", "Envoyée", "Livrée" ]],
                msg: "La valeur du statut est incorrecte."
            }
        }
    },
    utilisateurId: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let commandesModel = db.define('commandes', modelDefinition, modelOptions);

commandesModel.belongsTo(models.utilisateurs);

module.exports = commandesModel;