const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    quantiteCommandee: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let commandeContenuModel = db.define('commandeContenu', modelDefinition, modelOptions);

commandeContenuModel.belongsTo(models.produits);
commandeContenuModel.belongsTo(models.commandes);

module.exports = commandeContenuModel;