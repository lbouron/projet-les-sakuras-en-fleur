const Sequelize = require('sequelize');
const bcrypt = require('../utils/bcrypt');
const db = require('../services/sequelize');

let modelDefinition = {
    id: {
        type: Sequelize.DataTypes.STRING,
        defaultValue: Sequelize.DataTypes.UUIDV1,
        primaryKey: true,
        unique: true,
        allowNull: false
    },
    email: {
        type: Sequelize.DataTypes.STRING,
        unique: true,
        allowNull: true,
        validate: {
            isEmail: {
                msg: "L'email n'est pas au bon format."
            }
        }
    },
    password: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le mot de passe doit faire entre 1 et 250 caractères."
            }
        }
    },
    prenom: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le prénom doit faire entre 1 et 250 caractères."
            }
        }
    },
    nom: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le nom doit faire entre 1 et 250 caractères."
            }
        }
    },
    adresse: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "L'adresse doit faire entre 1 et 250 caractères."
            }
        }
    },
    codepostal: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            isNumeric: {
                msg: "Le code postal doit être numérique."
            },
            len: {
                args: [1, 250],
                msg: "Le code postal doit faire entre 1 et 250 caractères."
            }
        }
    },
    ville: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "La ville doit faire entre 1 et 250 caractères."
            }
        }
    },
    pays: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le pays doit faire entre 1 et 250 caractères."
            }
        }
    },
    isAdmin: {
        type: Sequelize.DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }
}

let modelOptions = {
    instanceMethods: {
        comparePasswords: bcrypt.comparePasswords
    },
    hooks: {
        beforeValidate: bcrypt.hashPassword
    },
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let utilisateursModel = db.define('utilisateurs', modelDefinition, modelOptions);

module.exports = utilisateursModel;