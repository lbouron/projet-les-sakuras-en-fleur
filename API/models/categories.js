const Sequelize = require('sequelize');
const db = require('../services/sequelize');
const models = require('./index');

const modelDefinition = {
    id: {
        type: Sequelize.DataTypes.BIGINT,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    libelle: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        validate: {
            len: {
                args: [1, 250],
                msg: "Le libellé doit faire entre 1 et 250 caractères."
            },
            notNull: {
                msg: "Le libellé ne doit pas être vide."
            },
            notEmpty: {
                msg: "Le libellé doit être renseigné."
            }
        }
    }
}

let modelOptions = {
    freezeTableName: true,
    paranoid: true,
    timestamps: true
}

let categoriesModel = db.define('categories', modelDefinition, modelOptions);

categoriesModel.hasMany(models.produits);

module.exports = categoriesModel;