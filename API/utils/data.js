const sequelize = require('../services/sequelize');
const models = require('../models');

async function importDefaultData() {
    await sequelize.sync();
    await importUtilisateur();
    await importCategories();
    await importProduits();
}

async function importUtilisateur() {
    if (!await models.utilisateurs.count()) {
        await models.utilisateurs.bulkCreate([
            {
                id: 'a0a4defa-f2ce-4963-9efc-86c716040cd4',
                email: 'admin@test.fr',
                password: '$2y$10$TDOunZGTy56KVhbfQCh4UOMMVg3gyerNPyTl5ZKUmmsXVK4OG10cG',
                isAdmin: true
            },
            {
                id: '9886a432-b52a-4eb5-b1f4-2d55a1341df3',
                email: 'user@test.fr',
                password: '$2y$10$TDOunZGTy56KVhbfQCh4UOMMVg3gyerNPyTl5ZKUmmsXVK4OG10cG',
                isAdmin: false
            }
        ]);
    }
}

async function importCategories() {
    if (!await models.categories.count()) {
        await models.categories.bulkCreate([
            { libelle: 'Fleurs' },
            { libelle: 'Graines' },
            { libelle: 'Terreaux' }
        ]);
    }
}

async function importProduits() {
    if (!await models.produits.count()) {
        await models.produits.bulkCreate([
            {
                libelle: 'Cerisier en pot',
                description: "Un magnifique cerisier en pot. Très facile à s'occuper.",
                prix: 30,
                categoryId: 1
            },
            {
                libelle: 'Graines de Lavatère',
                description: "De petites graines de Lavatère roses et blanches à planter dans votre jardin.",
                prix: 10,
                categoryId: 2
            },
            {
                libelle: 'Orchidée en pot',
                description: "Des orchidées violettes dans un pot. Elles garantissent d'éclaircir votre salon.",
                prix: 15,
                categoryId: 1
            },
            {
                libelle: 'Terreau pour fleurs',
                description: "Du terreau en sachet pour redonner vie à vos fleurs en pot.",
                prix: 10,
                categoryId: 3
            }
        ]);
    }
}

module.exports = {
    importDefaultData
}