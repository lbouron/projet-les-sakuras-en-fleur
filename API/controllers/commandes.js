const models = require('../models');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.commandes.findAll({
                order: [['createdAt', 'DESC']],
                attributes: {
                    exclude: ['deletedAt']
                },
                include: {
                    model: models.utilisateurs,
                    attributes: {
                        exclude: ['password', 'adresse', 'codepostal', 'ville', 'pays', 'isAdmin', 'createdAt', 'updatedAt', 'deletedAt']
                    }
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getCommandeById: async function (req, rep) {
        try {
            let result = await models.commandes.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['deletedAt']
                },
                include: {
                    model: models.utilisateurs,
                    attributes: {
                        exclude: ['password', 'adresse', 'codepostal', 'ville', 'pays', 'isAdmin', 'createdAt', 'updatedAt', 'deletedAt']
                    }
                }
            });

            if (result) {
                rep.status(200).json({ result: result, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            let { total, statut, utilisateurId, contenu } = req.body;

            if (!total || !contenu) {
                rep.status(400).json({ result: false, error: 'Veuillez renseigner les paramètres obligatoires.' });
            } else {
                let contenuValid = true;

                for (const element of contenu) {
                    if (!element.quantiteCommandee || !element.produitId) {
                        contenuValid = false;
                        console.log('Erreur dans le contenu.');
                        break;
                    }
                }

                if (!contenuValid) {
                    rep.status(400).json({ result: false, error: 'Veuillez renseigner les paramètres obligatoires.' });
                } else {
                    let newCommande = {
                        total: total,
                        statut: statut,
                        utilisateurId: utilisateurId
                    }

                    let newCommande_add = await models.commandes.create(newCommande);

                    for (const element of contenu) {
                        let newContenu = {
                            quantiteCommandee: element.quantiteCommandee,
                            produitId: element.produitId,
                            commandeId: newCommande_add.id
                        }

                        await models.commandeContenu.create(newContenu);
                    }

                    rep.status(201).json({ result: true, inserted: newCommande_add, error: '' });
                }
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            let { total, statut, utilisateurId } = req.body;
            let entry = await models.commandes.findOne({ where: { id: req.params.id } });

            if (entry) {
                if (total) entry.total = total;
                if (statut) entry.statut = statut;
                if (utilisateurId) entry.utilisateurId = utilisateurId;

                await entry.save();
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let result = models.commandes.destroy({ where: { id: req.params.id } });

            if (result) {
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getCommandesByUser: async function (req, rep) {
        try {
            let results = await models.commandes.findAll({
                where: { utilisateurId: req.user.id },
                order: [['createdAt', 'DESC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            console.log(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;