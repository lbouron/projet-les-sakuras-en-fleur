const models = require('../models');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.categories.findAll({
                order: [['libelle', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getCategorieById: async function (req, rep) {
        try {
            let result = await models.categories.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            if (result) {
                rep.status(200).json({ result: result, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            let instance = await models.categories.build(req.body);
            await instance.save();
            rep.status(201).json({ result: true, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            let { libelle } = req.body;
            let entry = await models.categories.findOne({ where: { id: req.params.id } });

            if (entry) {
                if (libelle) entry.libelle = libelle;

                await entry.save();
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let result = models.categories.destroy({ where: { id: req.params.id } });

            if (result) {
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;