const models = require('../models');
const Op = require('sequelize').Op;

let Controller = {
    signin: function (req, rep) {
        rep.json({ result: true, error: '' });
    },

    logout: function (req, rep, next) {
        req.logout(function (err) {
            if (err) { return next(err); }
            rep.status(200).json({ result: true, error: '' });
        });
    },

    userInfo: async function (req, rep) {
        rep.status(200).json({
            id: req.user.id,
            email: req.user.email,
            admin: req.user.isAdmin,
            prenom: req.user.prenom,
            nom: req.user.nom,
            adresse: req.user.adresse,
            codepostal: req.user.codepostal,
            ville: req.user.ville,
            pays: req.user.pays
        });
    },

    register: async function (req, rep) {
        let { email, password } = req.body;

        if (!email || !password) {
            rep.status(400).json({ result: false, error: 'Veuillez renseigner les champs obligatoires.' });
        } else {
            try {
                const exist = await models.utilisateurs.count({ where: { email: email, password: { [Op.ne]: null } } });

                if (!exist) {
                    let newUser = {
                        email: email,
                        password: password
                    };

                    await models.utilisateurs.create(newUser);
                    rep.status(201).json({ result: true, error: '' });
                } else {
                    rep.status(409).json({ result: false, error: 'L\'email est déjà utilisé.' });
                }
            } catch (error) {
                console.log(error);
                rep.status(500).json({ result: false, error: 'Erreur interne' });
            }
        }
    }
}

module.exports = Controller;