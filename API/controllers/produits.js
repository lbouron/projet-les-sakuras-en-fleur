const models = require('../models');
const fs = require('fs');

let Controller = {
    getAll: async function (req, rep) {
        try {
            let results = await models.produits.findAll({
                order: [['libelle', 'ASC']],
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            results.forEach(img => img.image = img.image !== null ? true : false);
            rep.status(200).json({ result: results, error: '' });
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getProduitById: async function (req, rep) {
        try {
            let result = await models.produits.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ['deletedAt']
                }
            });

            if (result) {
                
                rep.status(200).json({ result: result, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    add: async function (req, rep) {
        try {
            console.log(req.files, req.file, req.body);
            let { libelle, description, prix, categoryId } = req.body;

            if (!libelle) {
                rep.status(400).json({ result: false, error: 'Le produit doit avoir un nom.' });
            } else {
                let newProduct = {
                    libelle: libelle,
                    description: description,
                    prix: prix,
                    categoryId: categoryId
                };

                if (req.files) {
                    if (req.files.length > 0) {
                        newProduct.image = fs.readFileSync(req.files[0].path);
                        fs.unlinkSync(req.files[0].path);
                    }
                }

                await models.produits.create(newProduct);
                rep.status(201).json({ result: true, error: '' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    updateById: async function (req, rep) {
        try {
            if (!req.body.libelle) {
                rep.status(400).json({ result: false, error: 'Le produit doit avoir un nom.' });
            }

            let data = await models.produits.findOne({ where: { id: req.params.id } });

            if (data) {
                if (req.body.libelle) data.libelle = req.body.libelle;
                if (req.body.description) data.description = req.body.description;
                if (req.body.prix) data.prix = req.body.prix;
                if (req.body.categoryId) data.categoryId = req.body.categoryId;
                console.log(req.files);
                if (req.files?.length > 0) {
                    data.image = fs.readFileSync(req.files[0].path);
                    console.log(data.image);
                    fs.unlinkSync(req.files[0].path);
                }

                await data.save();
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            console.log(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    deleteById: async function (req, rep) {
        try {
            let result = models.produits.destroy({ where: { id: req.params.id } });

            if (result) {
                rep.status(200).json({ result: true, error: '' });
            } else {
                rep.status(404).json({ result: false, error: 'Aucun résultat pour cet identifiant.' });
            }
        } catch (error) {
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    },

    getImage: async function (req, rep) {
        try {
            let result = await models.produits.findOne({ where: { id: req.params.id } });

            rep.contentType('image/*');
            rep.send(result.image);
        } catch (error) {
            console.log(error);
            rep.status(500).json({ result: false, error: 'Erreur interne' });
        }
    }
}

module.exports = Controller;