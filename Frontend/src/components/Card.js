// Importation React
import { Link } from 'react-router-dom';

// Importation du css
import '../css/Card.css';

// Importation images
import noimage from '../img/noimage.png';

// Autres importations
import config from '../config';
const localhost = config.api.endpoint;

const Card = ({ produit, onClick }) => {
    return (
        <div className='divCard'>
            <Link
                to={{ pathname: `/products/details/${produit.id}` }}
                state={{ produit: produit }}
                className='produitLink'
            >
                {produit.image === true
                    ? <img src={`${localhost}/produits/image/${produit.id}`} crossOrigin='anonymous' alt={produit.libelle} />
                    : <img src={noimage} alt='Vide' />}

                <h2>{produit.libelle}</h2>

                <div>
                    <Link
                        to={{ pathname: `/products/details/${produit.id}` }}
                        state={{ produit: produit }}
                        className='detailsCard'
                    >
                        Détails du produit
                    </Link>

                    <p className='prixCard'><b>{parseFloat(produit.prix).toFixed(2)}€</b></p>
                </div>
            </Link>

            <button onClick={onClick}>Ajouter au panier</button>
        </div>
    );
}

export default Card;