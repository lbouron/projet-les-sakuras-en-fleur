// Importation React
import { Link } from 'react-router-dom';

// Importation du css
import '../css/Header.css';

// Importation logo
import logo from '../img/logo.png';

const Header = () => {
    // const user_id = sessionStorage.getItem('id');
    const user_admin = sessionStorage.getItem('admin');
    const user_email = sessionStorage.getItem('email');
    const user_cart = JSON.parse(sessionStorage.getItem('panier'));
    
    return (
        <header>
            <Link to='/' className='logo-center'>
                <img src={logo} className='logo' alt='Logo Les Sakuras en Fleur' />
            </Link>

                <nav>
                    <Link className='liens' to='/'>
                        <h1>Les Sakuras en Fleur</h1>
                    </Link>

                    <div>
                        <Link className='liens' to='/products'>Nos produits</Link>
                        <Link className='liens' to='/advices'>Nos conseils</Link>
                        
                        {(user_admin === 'true') &&
                            <Link className='liens' to='/admin'>Administration</Link>}
                        
                        {user_cart === null || user_cart.length === 0
                            ? <Link className='liens' to='/cart'>Panier</Link>
                            : <Link className='liens bold' to='/cart'>Panier ({user_cart.length})</Link>}
                        
                        {(user_email !== null)
                            ? <>
                                <Link className='liens' to='/profile'>Profil</Link>
                                <Link className='liens' to='/logout'>Déconnexion</Link>
                            </>
                            :
                            <>
                                <Link className='liens' to='/login'>Connexion</Link>
                                <Link className='liens' to='/register'>Inscription</Link>
                            </>}
                    </div>
                </nav>
        </header>
    );
}

export default Header;