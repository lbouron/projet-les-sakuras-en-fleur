// Importation du css
import '../css/Article.css';

// Importation images
import noimage from '../img/noimage.png';
import moins from '../img/square-minus-solid.svg';
import plus from '../img/square-plus-solid.svg';
import trash from '../img/trash-solid.svg';

// Autres importations
import config from '../config';
const localhost = config.api.endpoint;

const Article = ({ article, increment, decrement, deleteArticle }) => {
    return (
        <div className='divArticle'>
            {article.image === true
                ? <img src={`${localhost}/produits/image/${article.id}`} crossOrigin='anonymous' alt={article.libelle} />
                : <img src={noimage} alt='Aucun visuel' />}

            <h2>{article.libelle}</h2>

            <div>
                <button onClick={decrement}>
                    <img src={moins} alt='Quantité moins' />
                </button>
                
                <p>{article.quantite}</p>

                <button onClick={increment}>
                    <img src={plus} alt='Quantité plus' />
                </button>
            </div>

            <p>{parseFloat(article.prix * article.quantite).toFixed(2)}€</p>

            <button onClick={deleteArticle}>
                <img src={trash} alt="Supprimer l'article"  />
            </button>
        </div>
    );
}

export default Article;