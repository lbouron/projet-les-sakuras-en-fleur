// Importation React
import { Link } from 'react-router-dom';

// Importation du css
import '../css/Footer.css';

// Importation logo
import logo_footer from '../img/logo-footer.png';

const Footer = () => {
    return (
        <footer className='footer'>
            <Link to='/'>
                <img src={logo_footer} className='logo-footer' alt='Logo Les Sakuras en Fleur' />
            </Link>

            <p>Design by Léana Bouron - EANP2023 - LiveCampus</p>
        </footer>
    );
}

export default Footer;