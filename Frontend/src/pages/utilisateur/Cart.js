// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation des components
import Article from '../../components/Article';

// Importation du css
import '../../css/Cart.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Cart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            articles: [],
            count: 1,
            total: 0.0,
            user: sessionStorage.getItem('id'),
            adresse: sessionStorage.getItem('adresse'),
            popupConnexion: false,
            popupAdresse: false
        }
    }

    async componentDidMount() {
        await this.getCart();
        this.sommeTotale();
    }

    // Fonction pour incrémenter de 1 la quantité
    increment(id) {
        const newQuantite = [];

        this.state.articles.forEach(article => {
            if (article.id === id) {
                article.quantite += 1;
            }
            newQuantite.push(article);
        });

        this.setState({ articles: newQuantite }, () => this.sommeTotale());
    }

    // Fonction pour décrémenter de 1 la quantité (1 est le minimum)
    decrement(id) {
        const newQuantite = [];

        this.state.articles.forEach(article => {
            if (article.id === id) {
                if (article.quantite === 1) {
                    article.quantite = 1;
                } else {
                    article.quantite -= 1;
                }
            }
            newQuantite.push(article);
        });

        this.setState({ article: newQuantite }, () => this.sommeTotale());
    }

    render() {
        return (
            <main className='mainCart'>
                {this.state.popupConnexion === true &&
                <div className='popupCart'>
                    <p>Pour valider votre panier, connectez-vous !</p>

                    <Link to={{ pathname: '/login' }}>
                        <button>Connexion</button>
                    </Link>
                </div>}

                {this.state.popupAdresse === true &&
                <div className='popupCart'>
                    <p>Pour valider votre panier, renseignez votre adresse !</p>

                    <Link to={{ pathname: '/profile' }}>
                        <button>Mon profil</button>
                    </Link>
                </div>}
                
                <h1>Mon panier</h1>

                <div className='articlesCart'>
                    {this.state.articles.length > 0 ?
                        this.state.articles.map((article, index) => 
                            <Article
                                key={index}
                                article={article}
                                increment={() => this.increment(article.id)}
                                decrement={() => this.decrement(article.id)}
                                deleteArticle={() => this.deleteArticle(article.id)}
                            />
                        )
                        :
                        <p>Votre panier est vide.</p>}
                </div>
                
                <div className='totalCart'>
                    {this.state.articles.length > 0 &&
                    <p>
                        Total à payer ({this.state.articles.length === 1
                        ? `${this.state.articles.length} article`
                        : `${this.state.articles.length} articles`})
                        : {parseFloat(this.state.total).toFixed(2)}€
                    </p>}
                </div>

                <div className='buttonsCart'>
                    <Link to={{ pathname: '/' }}>
                        <button>Continuer mes achats</button>
                    </Link>

                    {this.state.articles.length > 0 &&
                    <button onClick={() => this.validateCart()}>Valider le panier</button>}
                </div>
            </main>
        );
    }

    // Fonction qui récupère les articles du panier
    getCart = async () => {
        try {
            if ((JSON.parse(sessionStorage.panier)).length > 0) {
                this.setState({ articles: JSON.parse(sessionStorage.panier) });
            } else {
                this.setState({ articles: [] });
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui calcule la somme totale
    sommeTotale = () => {
        let total = 0.0;

        this.state.articles.forEach((article) => {
            total += article.quantite * article.prix;
        });

        this.setState({ total: total });
    }

    // Fonction qui supprime un article du panier
    deleteArticle = async (id) => {
        try {
            let copyCart = JSON.parse(sessionStorage.getItem('panier'));

            copyCart = copyCart.filter(article => article.id !== id);

            sessionStorage.setItem('panier', JSON.stringify(copyCart));

            window.location.reload(true);
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui valide le panier
    validateCart = async () => {
        try {
            // Vérification que l'utilisateur est connecté
            if (this.state.user === null) {
                this.setState({ popupConnexion: true });
            } // Vérification que l'utilisateur connecté a renseigné son adresse
            if (this.state.user !== null && this.state.adresse === null) {
                this.setState({ popupAdresse: true });
            } // Utilisateur connecté & Adresse renseignée
            else {
                let contenu = [];
                this.state.articles.forEach((article) => {
                    let contenuToAdd = {
                        quantiteCommandee: article.quantite,
                        produitId: article.id
                    }
                    contenu.push(contenuToAdd);
                });

                let commande = {
                    total: this.state.total,
                    statut: "Validée",
                    utilisateurId: this.state.user,
                    contenu: contenu
                }

                let ajoutCommande = await this.addCommande(commande);

                if (ajoutCommande.success) {
                    sessionStorage.removeItem('panier');
                    window.location.pathname = '/';
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui crée une commande
    addCommande = async (commande) => {
        try {
            const URL = `/commandes/add`;

            const validation = await sakurasApiService.request({
                url: URL,
                method: 'PUT',
                data: commande
            });

            let insereeCommande = validation.data.inserted;
            let success = validation.data.result;

            return { commande: insereeCommande, success };
        } catch (error) {
            console.log(error);
        }
    }
}