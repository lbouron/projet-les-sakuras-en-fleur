// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Profile.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Password extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editUser: {
                password: null,
                newPwd: null,
                confirmNewPwd: null
            },
            isError: {
                password: null,
                newPwd: null,
                confirmNewPwd: null
            },
            error: null
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/profile' }} className='linkBack'>
                    ← Retour au profil
                </Link>

                <h1>Modifier mon mot de passe</h1>

                <form className='formProfile' onSubmit={this.editPwd}>
                    <input
                        type='password'
                        id='password'
                        placeholder='Votre mot de passe'
                        className={this.state.isError.password && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                password: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.password &&
                    <p>Le mot de passe ne peut pas être vide.</p>}

                    <input
                        type='password'
                        id='newPwd'
                        placeholder='Votre nouveau mot de passe'
                        className={this.state.isError.newPwd && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                newPwd: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.newPwd &&
                    <p>Le nouveau mot de passe ne peut pas être vide.</p>}

                    <input
                        type='password'
                        id='confirmNewPwd'
                        placeholder='Confirmez votre nouveau mot de passe'
                        className={this.state.isError.confirmNewPwd && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                confirmNewPwd: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.confirmNewPwd &&
                    <p>La confirmation du nouveau mot de passe ne peut pas être vide.</p>}

                    <p>{this.state.error}</p>

                    <input
                        type='submit'
                        value='Modifier le mot de passe'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour modifier le mot de passe
    editPwd = async (e) => {
        e.preventDefault();
        
        try {
            const URL = `/users/updatepwd/`;

            if (this.state.editUser.newPwd !== this.state.editUser.confirmNewPwd) {
                this.setState({
                    error: "Le mot de passe confirmé ne correspond pas au nouveau mot de passe."
                });
            } else {
                if (this.checkInputs()) {
                    let password = await sakurasApiService.request({
                        url: URL,
                        method: 'POST',
                        data: {
                            "password": this.state.editUser.password,
                            "newPwd": this.state.editUser.newPwd
                        }
                    });

                    if (password.data.result) {
                        window.location.pathname = '/profile';
                    } else {
                        throw new Error();
                    }
                } else {
                    console.log('Erreur dans les champs.');
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editUser } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editUser)) {
            switch (key) {
                case 'password':
                    isError[key] = value == null || value.length < 4;
                    break;
                case 'newPwd':
                    isError[key] = value == null || value.length < 8;
                    break;
                case 'confirmNewPwd':
                    isError[key] = value == null || value.length < 8;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}