// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Profile.css';

// Importations des images
import address from '../../img/house-solid.svg';
import password from '../../img/lock-solid.svg';
import trash from '../../img/trash-solid.svg';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: localStorage.getItem('id'),
            commandesByUser: []
        }
    }

    async componentDidMount() {
        await this.getCommandesByUser();
    }

    render() {
        return (
            <main>
                <h1>Mon profil</h1>

                <section className='profile'>
                    <h2>Gestion du profil</h2>

                    <div>
                        <Link to={{ pathname: '/profile/password' }}>
                            <img src={password} alt='Icône mot de passe' />

                            <p>Modifier mon mot de passe</p>
                        </Link>

                        <Link to={{ pathname: '/profile/address' }}>
                            <img src={address} alt='Icône adresse' />

                            <p>Modifier mon adresse</p>
                        </Link>

                        <Link onClick={this.deleteProfile}>
                            <img src={trash} alt='Icône suppression' />

                            <p>Supprimer mon compte</p>
                        </Link>
                    </div>
                </section>

                <section className='profile'>
                    <h2>Historique des commandes</h2>

                    <table>
                        <caption>Liste des commandes</caption>

                        <tr>
                            <th>Numéro de commande</th>
                            <th>Statut</th>
                            <th>Somme totale</th>
                        </tr>

                        {this.state.commandesByUser.length > 0 ?
                            this.state.commandesByUser.map((commande, index) =>
                            <tr key={index}>
                                <td>{commande.id}</td>
                                <td>{commande.statut}</td>
                                <td>{commande.total}€</td>
                            </tr>
                            )
                            :
                            <p>Aucune commande.</p>
                        }
                    </table>
                </section>
            </main>
        );
    }

    // Fonction qui récupère toutes les commandes de l'utilisateur connecté
    getCommandesByUser = async () => {
        try {
            const URL = `/commandes/user/${this.state.userId}`;

            const commandesByUser = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (commandesByUser.data.result) {
                this.setState({ commandesByUser: commandesByUser.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour supprimer le compte
    deleteProfile = async () => {
        try {
            const URL = `/users/delete/${this.state.userId}`;

            let suppression = await sakurasApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (suppression.data.result) {
                sessionStorage.clear();
                window.location.reload(true);
                window.location.pathname = '/login';
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }
}