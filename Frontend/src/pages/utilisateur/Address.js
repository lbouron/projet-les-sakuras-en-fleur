// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Profile.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Address extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editUser: {
                id: null,
                prenom: null,
                nom: null,
                adresse: null,
                codepostal: null,
                ville: null,
                pays: null
            },
            isError: {
                prenom: null,
                nom: null,
                adresse: null,
                codepostal: null,
                ville: null,
                pays: null
            }
        }
    }

    async componentDidMount() {
        await this.getUser();
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/profile' }} className='linkBack'>
                    ← Retour au profil
                </Link>

                <h1>Modifier mon adresse</h1>
                
                <form className='formProfile' onSubmit={this.editAddress}>
                    <input
                        type='text'
                        id='prenom'
                        placeholder='Votre prénom'
                        defaultValue={this.state.editUser.prenom}
                        className={this.state.isError.prenom && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                prenom: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.prenom &&
                    <p>Le prénom ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='nom'
                        placeholder='Votre nom'
                        defaultValue={this.state.editUser.nom}
                        className={this.state.isError.nom && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                nom: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.nom &&
                    <p>Le nom ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='adresse'
                        placeholder='Votre adresse'
                        defaultValue={this.state.editUser.adresse}
                        className={this.state.isError.adresse && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                adresse: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.adresse &&
                    <p>L'adresse ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='codepostal'
                        placeholder='Votre code postal'
                        defaultValue={this.state.editUser.codepostal}
                        className={this.state.isError.codepostal && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                codepostal: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.codepostal &&
                    <p>Le code postal ne peut pas être vide et doit être numérique.</p>}

                    <input
                        type='text'
                        id='ville'
                        placeholder='Votre ville'
                        defaultValue={this.state.editUser.ville}
                        className={this.state.isError.ville && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                ville: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.ville &&
                    <p>La ville ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='pays'
                        placeholder='Votre pays'
                        defaultValue={this.state.editUser.pays}
                        className={this.state.isError.pays && 'errorProfile'}
                        onChange={element => this.setState({
                            editUser: {
                                ...this.state.editUser,
                                pays: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.pays &&
                    <p>Le pays ne peut pas être vide.</p>}

                    <input
                        type='submit'
                        value="Modifier l'adresse"
                    />
                </form>
            </main>
        );
    }

    // Fonction pour récupérer les infos de l'utilisateur
    getUser = async () => {
        try {
            const userInfos = await sakurasApiService.request({
                url: '/userInfo',
                method: 'GET'
            });

            if (userInfos.statusText === 'OK') {
                this.setState({
                    editUser: {
                        ...this.state.editUser,
                        id: userInfos.data.id,
                        prenom: userInfos.data.prenom,
                        nom: userInfos.data.nom,
                        adresse: userInfos.data.adresse,
                        codepostal: userInfos.data.codepostal,
                        ville: userInfos.data.ville,
                        pays: userInfos.data.pays
                    }
                });
            } else {
                throw new Error();
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour modifier l'adresse
    editAddress = async (e) => {
        e.preventDefault();

        try {
            const URL = `/users/update/${this.state.id}`;

            if (this.checkInputs()) {
                let adresse = await sakurasApiService.request({
                    url: URL,
                    method: 'POST',
                    data: {
                        "prenom": this.state.editUser.prenom,
                        "nom": this.state.editUser.nom,
                        "adresse": this.state.editUser.adresse,
                        "codepostal": this.state.editUser.codepostal,
                        "ville": this.state.editUser.ville,
                        "pays": this.state.editUser.pays
                    }
                });

                if (adresse.data.result) {
                    sessionStorage.setItem('prenom', this.state.editUser.prenom);
                    sessionStorage.setItem('nom', this.state.editUser.nom);
                    sessionStorage.setItem('adresse', this.state.editUser.adresse);
                    sessionStorage.setItem('codepostal', this.state.editUser.codepostal);
                    sessionStorage.setItem('ville', this.state.editUser.ville);
                    sessionStorage.setItem('pays', this.state.editUser.pays);
                    
                    window.location.pathname = '/profile';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editUser } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editUser)) {
            switch (key) {
                case 'prenom':
                    isError[key] = value == null || value.length < 1;
                    break;
                case 'nom':
                    isError[key] = value == null || value.length < 1;
                    break;
                case 'adresse':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'codepostal':
                    isError[key] = value == null || isNaN(value) || value.length !== 5;
                    break;
                case 'ville':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'pays':
                    isError[key] = value == null || value.length < 2;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}