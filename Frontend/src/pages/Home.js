// Importations React
import React from 'react';

// Importations des components
import Card from '../components/Card';

// Importation du css
import '../css/Home.css';

// Autres importations
import { sakurasApiService } from '../services/sakurasApiService';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            produits: [],
            cart: []
        }
    }

    async componentDidMount() {
        await this.getProduits();
    }

    render() {
        return (
            <main>
                <h1>Bienvenue !</h1>

                <section className='produits'>
                    <h2>Les derniers produits ajoutés...</h2>

                    <div>
                        {this.state.produits.length > 0 ?
                            this.state.produits.map((produit, index) =>
                                <Card
                                    key={index}
                                    produit={produit}
                                    onClick={() => this.addToCart(produit)}
                                />
                            )
                        :
                        <p>Le magasin ne propose aucun produit.</p>}
                    </div>
                </section>
            </main>
        );
    }
    
    // Fonction qui récupère les produits
    getProduits = async () => {
        try {
            const URL = `/produits`;

            const produits = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (produits.data.result) {
                this.setState({ produits: produits.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui ajoute un article au panier
    addToCart = async (produit) => {
        try {
            const newProduit = {
                id: produit.id,
                libelle: produit.libelle,
                prix: produit.prix,
                quantite: 1,
                image: produit.image
            }

            if (sessionStorage.getItem('panier') === null) {
                this.state.cart.push(newProduit);

                sessionStorage.setItem('panier', JSON.stringify(this.state.cart));

                window.location.reload(true);
            } else {
                let copyCart = JSON.parse(sessionStorage.getItem('panier'));
                
                const itemIndex = copyCart.findIndex((article) => article.id === produit.id);

                if (itemIndex === -1) {
                    copyCart.push(newProduit);
                } else {
                    copyCart.map((article) =>
                        article.id === produit.id
                        ? article.quantite += 1
                        : article
                    )
                }

                sessionStorage.setItem('panier', JSON.stringify(copyCart));

                window.location.reload(true);
            }
        } catch (error) {
            console.log(error);
        }
    }
}