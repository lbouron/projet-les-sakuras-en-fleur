// Importations React
import React from 'react';

// Importation du css
import '../../css/Authentication.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: null,
            password: null,
            error: null
        }
    }

    render() {
        return (
            <main className='mainAuth'>
                <h1>Connexion</h1>

                <form className='formAuth' onSubmit={this.login}>
                    <p>{this.state.error}</p>

                    <input
                        type='email'
                        id='email'
                        placeholder='Email'
                        className={this.state.error !== null && 'colorError'}
                        onChange={element => this.setState({ email: element.target.value })}
                    />

                    <input
                        type='password'
                        id='password'
                        placeholder='Mot de passe'
                        className={this.state.error !== null && 'colorError'}
                        onChange={element => this.setState({ password: element.target.value })}
                    />

                    <input
                        type='submit'
                        value='Connexion'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour se connecter
    login = async (e) => {
        e.preventDefault();

        try {
            let login = await sakurasApiService.request({
                url: '/signin',
                method: 'POST',
                data: {
                    "email": this.state.email,
                    "password": this.state.password
                }
            });

            if (login.data.result) {
                let infos = await sakurasApiService.request({
                    url: '/userInfo',
                    method: 'GET'
                });

                if (infos.statusText === 'OK') {
                    sessionStorage.setItem('id', infos.data.id);
                    sessionStorage.setItem('email', infos.data.email);
                    sessionStorage.setItem('admin', infos.data.admin);
                    if (infos.data.prenom) sessionStorage.setItem('prenom', infos.data.prenom);
                    if (infos.data.nom) sessionStorage.setItem('nom', infos.data.nom);
                    if (infos.data.adresse) sessionStorage.setItem('adresse', infos.data.adresse);
                    if (infos.data.codepostal) sessionStorage.setItem('codepostal', infos.data.codepostal);
                    if (infos.data.ville) sessionStorage.setItem('ville', infos.data.ville);
                    if (infos.data.pays) sessionStorage.setItem('pays', infos.data.pays);
                } else {
                    throw new Error();
                }

                window.location.reload(true);
                window.location.pathname = '/';
            } else {
                throw new Error();
            }
        } catch (error) {
            if (error.response.status === 401) {
                this.setState({
                    error: "Identifiants incorrects."
                });
            } else {
                this.setState({
                    error: "Les champs doivent être remplis."
                });
                console.log(error);
            }
        }
    }
}