// Importations React
import React from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

class CategoryEdit extends React.Component {
    constructor(props) {
        super(props);

        this.location = props.location;

        this.state = {
            editCategory: {
                id: this.location.state.categorie.id,
                libelle: this.location.state.categorie.libelle
            },
            isError: {
                libelle: null
            }
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/admin' }} className='linkBack'>
                    ← Retour à l'administration
                </Link>

                <h1>Modifier une catégorie</h1>

                <form className='formAdmin' onSubmit={this.editCategorie}>
                    <input
                        type='text'
                        id='libelle'
                        placeholder='Nom de la catégorie'
                        defaultValue={this.state.editCategory.libelle}
                        className={this.state.isError.libelle && 'errorAdmin'}
                        onChange={element => this.setState({
                            editCategory: {
                                ...this.state.editCategory,
                                libelle: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.libelle &&
                    <p>Le libellé ne peut pas être vide.</p>}

                    <input
                        type='submit'
                        value='Modifier la catégorie'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour modifier une catégorie
    editCategorie = async (e) => {
        e.preventDefault();

        try {
            const URL = `/categories/update/${this.state.editCategory.id}`;

            if (this.checkInputs()) {
                let categorie = await sakurasApiService.request({
                    url: URL,
                    method: 'POST',
                    data: {
                        "libelle": this.state.editCategory.libelle
                    }
                });

                if (categorie.data.result) {
                    window.location.pathname = '/admin';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editCategory } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editCategory)) {
            switch (key) {
                case 'libelle':
                    isError[key] = value == null || value.length < 2;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}

export default () => (
    <CategoryEdit params={useParams()} location={useLocation()} />
);