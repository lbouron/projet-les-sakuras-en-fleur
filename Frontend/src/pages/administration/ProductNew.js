// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class ProductNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newProduct: {
                libelle: null,
                description: null,
                prix: null,
                categoryId: null,
                image: null
            },
            isError: {
                libelle: null,
                description: null,
                prix: null
            },
            categories: []
        }
    }

    async componentDidMount() {
        await this.getCategories();
    }

    // Fonction qui récupère toutes les catégories
    getCategories = async () => {
        try {
            const URL = `/categories`;

            const categories = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (categories.data.result) {
                this.setState({ categories: categories.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/admin' }} className='linkBack'>
                    ← Retour à l'administration
                </Link>

                <h1>Ajouter un produit</h1>

                <form className='formAdmin' encType='multipart/form-data' onSubmit={this.addProduit}>
                    <input
                        type='text'
                        id='libelle'
                        placeholder='Nom du produit'
                        className={this.state.isError.libelle && 'errorAdmin'}
                        onChange={element => this.setState({
                            newProduct: {
                                ...this.state.newProduct,
                                libelle: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.libelle &&
                    <p>Le libellé ne peut pas être vide.</p>}

                    <textarea
                        typeof='string'
                        id='description'
                        placeholder='Description du produit'
                        className={this.state.isError.description && 'errorAdmin'}
                        rows={10}
                        onChange={element => this.setState({
                            newProduct: {
                                ...this.state.newProduct,
                                description: element.target.value
                            }
                        })}
                    >
                    </textarea>

                    {this.state.isError.description &&
                    <p>La description ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='prix'
                        placeholder='Prix du produit'
                        className={this.state.isError.prix && 'errorAdmin'}
                        onChange={element => this.setState({
                            newProduct: {
                                ...this.state.newProduct,
                                prix: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.prix &&
                    <p>Le prix ne peut pas être vide et doit être un nombre.</p>}

                    <select
                        id='categorie-selection'
                        onChange={element => this.setState({
                            newProduct: {
                                ...this.state.newProduct,
                                categoryId: element.target.value
                            }
                        })}
                    >
                        <option value=''>Sélectionnez une catégorie</option>
                        {this.state.categories.length > 0 &&
                            this.state.categories.map((categorie, index) =>
                                <option key={index} value={categorie.id}>{categorie.libelle}</option>
                            )
                        }
                    </select>

                    <input
                        type='file'
                        name='image'
                        lang='fr'
                        accept='image/*'
                        onChange={element => this.setState({
                            newProduct: {
                                ...this.state.newProduct,
                                image: element.target.files[0]
                            }
                        })}
                    />

                    <input
                        type='submit'
                        value='Créer le produit'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour créer un produit
    addProduit = async (e) => {
        e.preventDefault();

        try {
            const URL = `/produits/add`;

            if (this.checkInputs()) {
                let formData = new FormData();
                formData.append('libelle', this.state.newProduct.libelle);
                formData.append('description', this.state.newProduct.description);
                formData.append('prix', this.state.newProduct.prix);
                formData.append('categoryId', this.state.newProduct.categoryId);
                formData.append('image', this.state.newProduct.image);

                const produit = await sakurasApiService.request({
                    url: URL,
                    method: 'PUT',
                    data: formData,
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                });

                if (produit.data.result) {
                    window.location.pathname = '/admin';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { newProduct } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(newProduct)) {
            switch (key) {
                case 'libelle':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'description':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'prix':
                    isError[key] = value == null || isNaN(value) || value.length < 1;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}