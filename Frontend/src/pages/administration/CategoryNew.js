// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class CategoryNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newCategory: {
                libelle: null
            },
            isError: {
                libelle: null
            }
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/admin' }} className='linkBack'>
                    ← Retour à l'administration
                </Link>

                <h1>Ajouter une catégorie</h1>

                <form className='formAdmin' onSubmit={this.addCategorie}>
                    <input
                        type='text'
                        id='libelle'
                        placeholder='Nom de la catégorie'
                        className={this.state.isError.libelle && 'errorAdmin'}
                        onChange={element => this.setState({
                            newCategory: {
                                ...this.state.newCategory,
                                libelle: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.libelle &&
                    <p>Le libellé ne peut pas être vide.</p>}

                    <input
                        type='submit'
                        value='Créer la catégorie'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour créer une catégorie
    addCategorie = async (e) => {
        e.preventDefault();

        try {
            const URL = `/categories/add`;

            if (this.checkInputs()) {
                const categorie = await sakurasApiService.request({
                    url: URL,
                    method: 'PUT',
                    data: {
                        "libelle": this.state.newCategory.libelle
                    }
                });

                if (categorie.data.result) {
                    window.location.pathname = '/admin';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { newCategory } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(newCategory)) {
            switch (key) {
                case 'libelle':
                    isError[key] = value == null || value.length < 2;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}