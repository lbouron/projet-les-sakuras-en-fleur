// Importations React
import React from 'react';
import { Link } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

export default class Admin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            produits: [],
            categories: [],
            commandes: []
        }
    }

    async componentDidMount() {
        await this.getProduits();
        await this.getCategories();
        await this.getCommandes();
    }

    render() {
        return (
            <main>
                <h1>Panneau d'administration</h1>

                <div className='flexAdmin'>
                    <section className='gestions'>
                        <h2>Gestion des produits</h2>

                        <table>
                            <caption>Liste des produits</caption>

                            <tr>
                                <th>Nom du produit</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>

                            {this.state.produits.length > 0 ?
                                this.state.produits.map((produit, index) =>
                                    <tr key={index}>
                                        <td>{produit.libelle}</td>
                                        
                                        <td>
                                            <Link
                                                to={{ pathname: `/product/update/${produit.id}` }}
                                                state={{ produit: produit }}
                                                className='linkAdmin'
                                            >
                                                Modifier
                                            </Link>
                                        </td>
                                        
                                        <td>
                                            <button onClick={() => this.deleteProduit(produit.id)}>
                                                Supprimer
                                            </button>
                                        </td>
                                    </tr>
                                )
                                :
                                <p>Aucun produit.</p>
                            }
                        </table>

                        <button>
                            <Link to={{ pathname: `/product/add` }}>
                                Ajouter un produit
                            </Link>
                        </button>
                    </section>

                    <section className='gestions'>
                        <h2>Gestion des catégories</h2>

                        <table>
                            <caption>Liste des catégories</caption>

                            <tr>
                                <th>Nom de la catégorie</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>

                            {this.state.categories.length > 0 ?
                                this.state.categories.map((categorie, index) =>
                                    <tr key={index}>
                                        <td>{categorie.libelle}</td>
                                        
                                        <td>
                                            <Link
                                                to={{ pathname: `/category/update/${categorie.id}` }}
                                                state={{ categorie: categorie }}
                                                className='linkAdmin'
                                            >
                                                Modifier
                                            </Link>
                                        </td>
                                        
                                        <td>
                                            <button onClick={() => this.deleteCategorie(categorie.id)}>
                                                Supprimer
                                            </button>
                                        </td>
                                    </tr>
                                )
                                :
                                <p>Aucune catégorie.</p>}
                        </table>

                        <button>
                            <Link to={{ pathname: `/category/add` }}>
                                Ajouter une catégorie
                            </Link>
                        </button>
                    </section>
                </div>

                <section className='gestions'>
                    <h2>Gestion des commandes</h2>

                    <table>
                        <caption>Liste des commandes</caption>

                        <tr>
                            <th>Numéro de commande</th>
                            <th>Statut</th>
                            <th>Somme totale</th>
                            <th>Utilisateur</th>
                            <th>Modifier</th>
                        </tr>

                        {this.state.commandes.length > 0 ?
                            this.state.commandes.map((commande, index) =>
                                <tr key={index}>
                                    <td>{commande.id}</td>
                                    <td>{commande.statut}</td>
                                    <td>{commande.total}€</td>
                                    <td>{commande.utilisateur.prenom === null || commande.utilisateur.nom === null
                                        ? `${commande.utilisateur.email}`
                                        : `${commande.utilisateur.prenom} ${commande.utilisateur.nom}`}</td>
                                    <td>
                                        <Link
                                            to={{ pathname: `/order/update/${commande.id}` }}
                                            state={{ commande: commande }}
                                            className='linkAdmin'
                                        >
                                            Modifier
                                        </Link>
                                    </td>
                                </tr>
                            )
                            :
                            <p>Aucune commande.</p>
                        }
                    </table>
                </section>
            </main>
        );
    }

    // Fonction qui récupère tous les produits
    getProduits = async () => {
        try {
            const URL = `/produits`;

            const produits = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (produits.data.result) {
                this.setState({ produits: produits.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui supprime un produit
    deleteProduit = async (id) => {
        try {
            const URL = `/produits/delete/${id}`;

            let produit = await sakurasApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (produit.data.result) {
                window.location.reload(true);
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui récupère toutes les catégories
    getCategories = async () => {
        try {
            const URL = `/categories`;

            const categories = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (categories.data.result) {
                this.setState({ categories: categories.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui supprime une catégorie
    deleteCategorie = async (id) => {
        try {
            const URL = `/categories/delete/${id}`;

            let categorie = await sakurasApiService.request({
                url: URL,
                method: 'DELETE'
            });

            if (categorie.data.result) {
                window.location.reload(true);
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction qui récupère toutes les commandes
    getCommandes = async () => {
        try {
            const URL = `/commandes`;

            const commandes = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (commandes.data.result) {
                this.setState({ commandes: commandes.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }
}