// Importations React
import React from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';
import config from '../../config';
const localhost = config.api.endpoint;

class ProductEdit extends React.Component {
    constructor(props) {
        super(props);

        this.location = props.location;

        this.state = {
            editProduct: {
                id: this.location.state.produit.id,
                libelle: this.location.state.produit.libelle,
                description: this.location.state.produit.description,
                prix: this.location.state.produit.prix,
                categoryId: this.location.state.produit.categoryId,
                image: this.location.state.produit.image
            },
            isError: {
                libelle: null,
                description: null,
                prix: null
            },
            categories: []
        }
    }

    async componentDidMount() {
        await this.getCategories();
    }

    // Fonction qui récupère toutes les catégories
    getCategories = async () => {
        try {
            const URL = `/categories`;

            const categories = await sakurasApiService.request({
                url: URL,
                method: 'GET'
            });

            if (categories.data.result) {
                this.setState({ categories: categories.data.result });
            } else {
                console.log('Erreur');
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/admin' }} className='linkBack'>
                    ← Retour à l'administration
                </Link>

                <h1>Modifier un produit</h1>

                <form className='formAdmin' encType='multipart/form-data' onSubmit={this.editProduit}>
                    <input
                        type='text'
                        id='libelle'
                        placeholder='Nom du produit'
                        defaultValue={this.state.editProduct.libelle}
                        className={this.state.isError.libelle && 'errorAdmin'}
                        onChange={element => this.setState({
                            editProduct: {
                                ...this.state.editProduct,
                                libelle: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.libelle &&
                    <p>Le libellé ne peut pas être vide.</p>}

                    <textarea
                        typeof='string'
                        id='description'
                        placeholder='Description du produit'
                        defaultValue={this.state.editProduct.description}
                        className={this.state.isError.description && 'errorAdmin'}
                        rows={10}
                        onChange={element => this.setState({
                            editProduct: {
                                ...this.state.editProduct,
                                description: element.target.value
                            }
                        })}
                    >
                    </textarea>

                    {this.state.isError.description &&
                    <p>La description ne peut pas être vide.</p>}

                    <input
                        type='text'
                        id='prix'
                        placeholder='Prix du produit'
                        defaultValue={this.state.editProduct.prix}
                        className={this.state.isError.prix && 'errorAdmin'}
                        onChange={element => this.setState({
                            editProduct: {
                                ...this.state.editProduct,
                                prix: element.target.value
                            }
                        })}
                    />

                    {this.state.isError.prix &&
                    <p>Le prix ne peut pas être vide et doit être un nombre.</p>}

                    <select
                        id='categorie-selection'
                        defaultValue={this.state.editProduct.categoryId}
                        onChange={element => this.setState({
                            editProduct: {
                                ...this.state.editProduct,
                                categoryId: element.target.value
                            }
                        })}
                    >
                        <option value=''>Sélectionnez une catégorie</option>
                        {this.state.categories.length > 0 &&
                            this.state.categories.map((categorie, index) =>
                                <option key={index} value={categorie.id}>{categorie.libelle}</option>
                            )}
                    </select>

                    <input
                        type='file'
                        name='image'
                        lang='fr'
                        accept='image/*'
                        onChange={element => this.setState({
                            editProduct: {
                                ...this.state.editProduct,
                                image: element.target.files[0]
                            }
                        })}
                    />

                    {this.state.editProduct.image &&
                    <img src={`${localhost}/produits/image/${this.state.editProduct.id}`} crossOrigin='anonymous' alt={this.state.editProduct.libelle} />}

                    <input
                        type='submit'
                        value='Modifier le produit'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour modifier un produit
    editProduit = async (e) => {
        e.preventDefault();

        try {
            const URL = `/produits/update/${this.state.editProduct.id}`;

            if (this.checkInputs()) {
                let formData = new FormData();
                formData.append('libelle', this.state.editProduct.libelle);
                formData.append('description', this.state.editProduct.description);
                formData.append('prix', this.state.editProduct.prix);
                formData.append('categoryId', this.state.editProduct.categoryId);
                formData.append('image', this.state.editProduct.image);

                const produit = await sakurasApiService.request({
                    url: URL,
                    method: 'POST',
                    data: formData,
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                });

                if (produit.data.result) {
                    window.location.pathname = '/admin';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editProduct } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editProduct)) {
            switch (key) {
                case 'libelle':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'description':
                    isError[key] = value == null || value.length < 2;
                    break;
                case 'prix':
                    isError[key] = value == null || isNaN(value) || value.length < 1;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}

export default () => (
    <ProductEdit params={useParams()} location={useLocation()} />
);