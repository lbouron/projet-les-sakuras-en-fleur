// Importations React
import React from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';

// Importation du css
import '../../css/Admin.css';

// Autres importations
import { sakurasApiService } from '../../services/sakurasApiService';

class OrderEdit extends React.Component {
    constructor(props) {
        super(props);

        this.location = props.location;

        this.state = {
            editOrder: {
                id: this.location.state.commande.id,
                total: this.location.state.commande.total,
                statut: this.location.state.commande.statut,
                utilisateurId: this.location.state.commande.utilisateurId,
                utilisateurEmail: this.location.state.commande.utilisateur.email,
                utilisateurPrenom: this.location.state.commande.utilisateur.prenom,
                utilisateurNom: this.location.state.commande.utilisateur.nom
            },
            isError: {
                statut: null
            }
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/admin' }} className='linkBack'>
                    ← Retour à l'administration
                </Link>

                <h1>Modifier une commande</h1>

                <form className='formAdmin' onSubmit={this.editOrder}>
                    <input
                        type='text'
                        id='total'
                        placeholder='Somme totale de la commande'
                        defaultValue={this.state.editOrder.total}
                        disabled
                    />

                    <select
                        id='statut-selection'
                        className={this.state.isError.statut && 'errorAdmin'}
                        defaultValue={this.state.editOrder.statut}
                        onChange={element => this.setState({
                            editOrder: {
                                ...this.state.editOrder,
                                statut: element.target.value
                            }
                        })}
                    >
                        <option value=''>Sélectionnez un statut</option>
                        <option value='Validée'>Validée</option>
                        <option value='Payée'>Payée</option>
                        <option value='Envoyée'>Envoyée</option>
                        <option value='Livrée'>Livrée</option>
                        <option value='Annulée'>Annulée</option>
                    </select>

                    {this.state.isError.statut &&
                    <p>Un statut doit être sélectionné.</p>}

                    <input
                        type='text'
                        id='utilisateur'
                        placeholder={`Nom de l'utilisateur`}
                        defaultValue={this.state.editOrder.utilisateurNom !== null && this.state.editOrder.utilisateurPrenom !== null
                                        ? `${this.state.editOrder.utilisateurPrenom} ${this.state.editOrder.utilisateurNom}`
                                        : this.state.editOrder.utilisateurEmail}
                        disabled
                    />

                    <input
                        type='submit'
                        value='Modifier la commande'
                    />
                </form>
            </main>
        );
    }

    // Fonction pour modifier une commande
    editOrder = async (e) => {
        e.preventDefault();

        try {
            const URL = `/commandes/update/${this.state.editOrder.id}`;

            if (this.checkInputs()) {
                let commande = await sakurasApiService.request({
                    url: URL,
                    method: 'POST',
                    data: {
                        "statut": this.state.editOrder.statut
                    }
                });

                if (commande.data.result) {
                    window.location.pathname = '/admin';
                } else {
                    throw new Error();
                }
            } else {
                console.log('Erreur dans les champs.');
            }
        } catch (error) {
            console.log(error);
        }
    }

    // Fonction pour vérifier les inputs
    checkInputs = () => {
        let { isError } = this.state;
        let { editOrder } = this.state;
        let inputsCorrects = true;

        for (const [key, value] of Object.entries(editOrder)) {
            switch (key) {
                case 'statut':
                    isError[key] = value == null || value.length < 2;
                    break;
                default:
                    break;
            }
        }
        this.setState({ isError });

        // Retour false si un des champs est incorrect
        for (const [key, value] of Object.entries(isError)) {
            if (value) inputsCorrects = false;
            if (key) { }
        }

        return inputsCorrects;
    }
}

export default () => (
    <OrderEdit params={useParams()} location={useLocation()} />
);