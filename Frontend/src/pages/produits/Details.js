// Importations React
import React from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';

// Importation du css
import '../../css/Details.css';

// Importation images
import noimage from '../../img/noimage.png';
import config from '../../config';
const localhost = config.api.endpoint;

class Details extends React.Component {
    constructor(props) {
        super(props);

        this.location = props.location;

        this.state = {
            produit: {
                id: this.location.state.produit.id,
                libelle: this.location.state.produit.libelle,
                description: this.location.state.produit.description,
                prix: this.location.state.produit.prix,
                image: this.location.state.produit.image
            },
            cart: []
        }
    }

    render() {
        return (
            <main>
                <Link to={{ pathname: '/' }} className='linkDetails'>
                    ← Retour à l'accueil
                </Link>

                <section className='rowDetails'>
                    {this.state.produit.image === true
                        ? <img src={`${localhost}/produits/image/${this.state.produit.id}`} crossOrigin='anonymous' alt={this.state.produit.libelle} />
                        : <img src={noimage} alt='Vide' />}

                    <div>
                        <h1>{this.state.produit.libelle}</h1>

                        <p className='descrip'><b>Description</b></p>
                        <p>{this.state.produit.description}</p>

                        <p><b>{parseFloat(this.state.produit.prix).toFixed(2)}€</b></p>

                        <button onClick={() => this.addToCart()}>+ Ajouter au panier</button>
                    </div>
                </section>
            </main>
        );
    }

    // Fonction qui ajoute un article au panier
    addToCart = async () => {
        try {
            const newProduit = {
                id: this.state.produit.id,
                libelle: this.state.produit.libelle,
                prix: this.state.produit.prix,
                quantite: 1,
                image: this.state.produit.image
            }
            
            if (sessionStorage.getItem('panier') === null) {
                this.state.cart.push(newProduit);

                sessionStorage.setItem('panier', JSON.stringify(this.state.cart));

                window.location.reload(true);
            } else {
                let copyCart = JSON.parse(sessionStorage.getItem('panier'));

                const itemIndex = copyCart.findIndex((article) => article.id === this.state.produit.id);

                if (itemIndex === -1) {
                    copyCart.push(newProduit);
                } else {
                    copyCart.map((article) =>
                        article.id === this.state.produit.id
                        ? article.quantite += 1
                        : article
                    )
                }

                sessionStorage.setItem('panier', JSON.stringify(copyCart));

                window.location.reload(true);
            }
        } catch (error) {
            console.log(error);
        }
    }
}

export default () => (
    <Details params={useParams()} location={useLocation()} />
);