// Importations React
import { BrowserRouter, Route, Routes } from 'react-router-dom';

// Importations des pages
import Home from './pages/Home';
import Details from './pages/produits/Details';
import Products from './pages/produits/Products';
import Advices from './pages/conseils/Advices';
import Cart from './pages/utilisateur/Cart';
import Profile from './pages/utilisateur/Profile';
import Address from './pages/utilisateur/Address';
import Password from './pages/utilisateur/Password';
import Admin from './pages/administration/Admin';
import ProductNew from './pages/administration/ProductNew';
import ProductEdit from './pages/administration/ProductEdit';
import CategoryNew from './pages/administration/CategoryNew';
import CategoryEdit from './pages/administration/CategoryEdit';
import OrderEdit from './pages/administration/OrderEdit';
import Login from './pages/authentification/Login';
import Logout from './pages/authentification/Logout';
import Register from './pages/authentification/Register';

// Importation des components
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <BrowserRouter>
      <Header />

      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/products/details/:id' element={<Details />} />
        <Route path='/products' element={<Products />} />
        <Route path='/advices' element={<Advices />} />
        <Route path='/cart' element={<Cart />} />
        <Route path='/profile' element={<Profile />} />
        <Route path='/profile/address' element={<Address />} />
        <Route path='/profile/password' element={<Password />} />
        <Route path='/admin' element={<Admin />} />
        <Route path='/product/add' element={<ProductNew />} />
        <Route path='/product/update/:id' element={<ProductEdit />} />
        <Route path='/category/add' element={<CategoryNew />} />
        <Route path='/category/update/:id' element={<CategoryEdit />} />
        <Route path='/order/update/:id' element={<OrderEdit />} />
        <Route path='/login' element={<Login />} />
        <Route path='/logout' element={<Logout />} />
        <Route path='/register' element={<Register />} />
      </Routes>

      <Footer />
    </BrowserRouter>
  );
}

export default App;
