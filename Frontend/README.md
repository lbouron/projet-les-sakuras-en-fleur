# Les Sakuras en Fleur - FrontEnd

## Lancement
Taper `npm start` pour démarrer le FrontEnd. Il est possible qu'il demande à se lancer sur un autre port, car le port 3000 sera utilisé par l'API.

## Connexion
Après que l'API ait été démarrée pour la première fois, deux utilisateurs (Administrateur et Utilisateur) sont créés :

`Admin`
email : admin@test.fr
mdp : test

`Utilisateur`
email : user@test.fr
mdp : test